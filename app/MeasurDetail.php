<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MeasurDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'host_id',
        'total_time',
        'redirect_count',
    ];
 
    public function host() {
        return $this->belongsTo(Host::class);
    }
}