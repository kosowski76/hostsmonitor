<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Host;

class HostController extends Controller
{
    // 
    private $mStatus;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $hosts = Host::all();

        return response()->json([
            'error' => false,
            'hosts'  => $hosts,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $hosts = array();
        $items = $request->input('items');

        foreach($items as $item)
        {
            $count = DB::table('hosts')->where('name', $item['name'])->count();

            if($count === 0)
            {
                $host = Host::create(['name' => $item['name']]);
                array_push($hosts, $host);
            }           
        }

        return response()->json([
            'error' => false,
            'hosts'  => $hosts,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tenMeasur($url)
    {
      /*  $sql = "SELECT id FROM hosts WHERE name = '". $url ."'";
        $host_id = DB::select($sql); */

       $host_id = DB::table('hosts')->where('name', $url)->count();

        if($host_id != 0 || $host_id != null)
        {
            // get last ten records            
            $last_ten_details = DB::table('hosts')
                 ->join('measur_details', 'hosts.id', '=', 'measur_details.host_id')
                 ->select('hosts.name', 'measur_details.total_time', 'measur_details.redirect_count', 
                          'measur_details.status', 'measur_details.created_at')
                 ->where('hosts.name', '=', $url)->orderBy('measur_details.id', 'desc')
                 ->limit(10)->get();

            return response()->json([
                            'error' => false,
                             'host' => $url,
                  'result_last_ten' => $last_ten_details,
                ], 200);
        }
        else
        {
            return response()->json([
                'error' => false,
                'host' => $url,
                'message' => 'host: '. $url .' not exist in database!',
           ], 200);
        }
    }

    private function checkUrl($url) 
    {
        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt ($ch, CURLOPT_URL, $url);
        //curl_setopt ($ch, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt ($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        
        // grab URL and pass it to the browser
        curl_exec($ch);

        // get status connection
        $this->mStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
         
        // close cURL resource, and free up system resources
        curl_close($ch);
    }


}
