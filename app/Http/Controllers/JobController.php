<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Jobs\CheckHost;

class JobController extends Controller
{
    //
    public function processQueue()
    {    
        // while - niekończąca się
        // for - niekończąca się
        for($i=0; $i<20; $i++)
        {
          $hostJob = new CheckHost();
          dispatch($hostJob);
          sleep(60);
        }
    }
}
