<?php
//
namespace App\Custom;

use App\Custom\HostMonitor;
use App\MeasurDetail;

class HostMonitor
{
    private $mHostId;
    private $mHostName;
    private $mStatus;
    private $mTotalTime;
    private $mRedirectCount;

    function __construct($hostId, $hostName) {
        //
        $this->mHostId = $hostId;
        $this->mHostName = $hostName;
        $this->checkUrl();
    }

    private function checkUrl() {
        // create a new cURL resource
        $ch = curl_init();

        // set URL and other appropriate options
        curl_setopt ($ch, CURLOPT_URL, $this->mHostName);
        //curl_setopt ($ch, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt ($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        
        // grab URL and pass it to the browser
        curl_exec($ch);

        // get status connection
        $this->mStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->mTotalTime = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
        $this->mRedirectCount = curl_getinfo($ch, CURLINFO_REDIRECT_COUNT);
 
        // close cURL resource, and free up system resources
        curl_close($ch);
        }

   //
   public function checkMeasure()
   {   
        
    // sprawdzenie adresu
    if ($this->mStatus != null)
    {
       $measurDetail = new MeasurDetail();
       $measurDetail->host_id = $this->mHostId;
       $measurDetail->total_time = $this->mTotalTime;
       $measurDetail->redirect_count = $this->mRedirectCount;
       $measurDetail->status = $this->mStatus;

       $measur = $measurDetail->save();
       
       // sprawdzenie czy wpis został zapisany
       if(!$measur) { App::abort(500, 'Error') ; }

    }
     // echo 'Error #' . curl_errno($ch) . ': ' . curl_error($ch);    
    
   }

}