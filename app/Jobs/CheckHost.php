<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Custom\HostMonitor;
use App\Host;

class CheckHost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // pobranie listy hostow z bazy danych
        $hosts = Host::all();

        // petla do pobierania hostId, hostName z bazy danych hosts
        foreach($hosts as $host)
        {
          // create the instance of HostMonitor and get hostId, hostName
          $checkHost = new HostMonitor($host['id'], $host['name']);

          // check the measure and insert to database
          $checkHost->checkMeasure();
        }        
    }
    
}
