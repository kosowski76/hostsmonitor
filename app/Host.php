<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Host extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
    ];

    public function details() {
        return $this->hasMany(MeasurDetail::class);
    }
}