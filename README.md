# hostsMonitor

Środowisko:

Linux Debian 10 Buster
PHP 7.3 (z curl)
Mysql 8
Laravel 6 ( queue:work z nadzorcą Supervisor)
Redis 5.0.3


wersję demonstracyjną można zobaczyć 
GET  http://hostsmonitor.devst3x.webd.pl/api/v1/monitors


dodawanie kilku hostów/urls 
konfiguracja headera: 
Accept: application/json
Content-Type: application/json, 
za pomocą reguły:

{
"items":[ 
{ "name": "www.examle.com" },
{ "name": "www.example_2.com" },
{ "name": "www.example.net" } ]
}

bez przedrostka 'http://' - oczywiście można wykonać walidację i z automatu go usuwać,
jeżeli jakiś adres jest już w bazie nie jest ponownie dodawany,

status połączenia z hostem jest rejestrowany przy wykonaniu pomiaru,

dodawanie hostów:  
POST  http://hostsmonitor.devst3x.webd.pl/api/v1/monitors


w tej wersji demo pomiary wykonywałem za pomocą kolejkowania laravel queues z nadzorcą Supervisor,
z cache'walną bazą Redis, a wyniki zapisywane na serverze MySql, w ilości 20 co 60 sekund,
wyświetlane po 10 ostatnich wyników, 
w przypadku gdy wystąpi zapytanie o hosta, który nie widnieje w bazie danych 
aplikacja wyśle odpowiedni komunikat:

GET  http://hostsmonitor.devst3x.webd.pl/api/v1/monitors/{url}
np. http://hostsmonitor.devst3x.webd.pl/api/v1/monitors/www.google.com
